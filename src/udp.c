/*
 * Asterisk -- An open source telephony toolkit.
 *
 * Copyright (C) 2011, SoftHouse
 *
 * Dimitar Dimitrov <dimitar.dimitrov@softhouse.bg>
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
#define PASSKEY "blowfish"
#define SEND_TIMES 3

#include "global.h"
#include "app_notify.h"

int send_notify(char* ip, int port, char* message)
{
	struct sockaddr_in si_other;
	int i, j, s, slen = sizeof(si_other);

	for(i = 0, j = 0; i < strlen(message); i++)
		message[i] = (message[i] ^ PASSKEY[(j > strlen(PASSKEY) - 1)?j=0:j++]) + 32;

	if ( (s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
 		perror("socket");

	memset((char *)&si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(port);

	if (inet_aton(ip, &si_other.sin_addr) == 0) {
		ast_log(LOG_WARNING, "inet_aton() failed!\n");
		return -1;
	}

	for (i = 0; i < SEND_TIMES; i++) { 
		if (sendto(s, message, strlen(message), 0, (const struct sockaddr *)&si_other, slen)==-1)
			ast_log(LOG_WARNING, "Error sending message [%s] to server[%s]!\n", message, ip);
	}

	close(s);
	return 0;
}
