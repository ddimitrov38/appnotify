/*
 * Asterisk -- An open source telephony toolkit.
 *
 * Copyright (C) 2011, SoftHouse
 *
 * Dimitar Dimitrov <dimitar.dimitrov@softhouse.bg>
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*! 
 * \file
 * \brief Asterisk notification module. 
 *
 * \author\verbatim Dimiar Dimitrov <dimitar.dimitrov@softhouse.bg> \endverbatim
 * 
 * This is a module for sending notifications on incoming or outgoing calls to a user system via UDP packet 
 * \ingroup notifications
 */

#ifndef AST_MODULE
#define AST_MODULE "app_notify"
#endif

#include "global.h"
#include "app_notify.h"

ASTERISK_FILE_VERSION(__FILE__, "$Revision: 272531 $")

static char *app_notify	= "UDPNotify";
static char *synopsis	= "UDP Notification System";
static char *descrip	= "UDPNotify([IP][PORT][MESSAGE]):  Send MESSAGE to IP on UDP PORT.\n";


static int notify_exec(struct ast_channel *chan, void *data)
{
	char* parse;

	AST_DECLARE_APP_ARGS(args,
		AST_APP_ARG(IP);
		AST_APP_ARG(Port);
		AST_APP_ARG(Message);
	);

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "%s: Missing arguments!\n", app_notify);
		return -1;
	}

	parse = ast_strdupa(data);
	AST_STANDARD_APP_ARGS(args, parse);

	send_notify(args.IP, atoi(args.Port), args.Message);

	return 0;
}


static int unload_module(void)
{
	ast_log(LOG_NOTICE, "UDP Notification module unloaded!\n");

	return ast_unregister_application(app_notify);
}


static int load_module(void)
{
	ast_log(LOG_NOTICE, "UDP notification module loaded!\n");

	return ast_register_application(app_notify, notify_exec, synopsis, descrip)? AST_MODULE_LOAD_DECLINE : AST_MODULE_LOAD_SUCCESS;
}

AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, "UDP notificaion application module");
