/*************************************************************************
 *
 * (C) 2011 SoftHouse - Sofia/Bulgaria - <office@minitelecom.org>
 *  by Dimitar Dimitrov <dimitar.dimitrov@softhouse.bg>
 *
 *************************************************************************/
#ifndef __APP_NOTIFY_H
#define __APP_NOTIFY_H

int send_notify(char*, int, char*);

#endif
