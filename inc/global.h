/*************************************************************************
 *
 * (C) 2011 SoftHouse - Sofia/Bulgaria - <office@minitelecom.org>
 *  by Dimitar Dimitrov <dimitar.dimitrov@softhouse.bg>
 *
 *************************************************************************/
#ifndef __GLOBAL_H
#define __GLOBAL_H

// Do not include config becase its used by Asterisk and cause warnings on compile
// #include "config.h"

#include <asterisk.h>

#include <asterisk/file.h>
#include <asterisk/logger.h>
#include <asterisk/channel.h>
#include <asterisk/pbx.h>
#include <asterisk/module.h>
#include <asterisk/lock.h>
#include <asterisk/app.h>

#endif
